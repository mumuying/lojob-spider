# 用于将爬取到的结果合并成统一的文件，用于全量上传MeiliSearch
# 增量上传的问题是需要频繁范围meilisearch，导致连接失败，所以改为仅支持全量上传
import json
import datetime

# 已有结果文件
oldRetFile = "D:/workspace/loaf-on-job/02.code/lojob-spider/book/json/result-20240305150802.json"
# 待合并文件
doRetFile = "D:/workspace/loaf-on-job/02.code/lojob-spider/book/crawl-result-20240305155844.json"
# 合并结果文件
retFile = "D:/workspace/loaf-on-job/02.code/lojob-spider/book/json/result-" + datetime.datetime.now().strftime('%Y%m%d%H%M%S') + ".json"
# 待合并网站的权重，用于计算评分
doWeight = 1
# 待合并网站名称
doName = "笔趣阁"

# 网站信息
class WebSite(object):
    # 网站名称，使用简写，例如知轩藏书、起点中文网等等
    name = ""
    # 评分，本网站对电子书的评分
    appraise = 0.0
    # 具体电子书对应的网址
    url = ""
    # 具体电子书对应的下载地址
    downloadUrl = ""
    # 校对（校对版、整理未校对精校版全本等等，不是很重要）
    collation = ""
    # 权重，用于计算评分
    weight = 1

    def __init__(self, name, appraise, url, downloadUrl, collation, weight):
        self.name=name
        self.appraise=appraise
        self.url=url
        self.downloadUrl=downloadUrl
        self.collation=collation
        self.weight=weight

# 自定义JSON编码器，处理WebSite类
def custom_encoder(obj):
    if isinstance(obj, WebSite):
        return {'name': obj.name, 'appraise': obj.appraise, 'url': obj.url, 'downloadUrl': obj.downloadUrl, 'collation': obj.collation, 'weight': obj.weight}
    else:
        return obj
def custom_decoder(obj):
    if 'name' in obj:
        return WebSite(obj['name'], obj['appraise'], obj['url'], obj['downloadUrl'], obj['collation'], obj['weight'])
    else:
        return obj

def main():
    # 已有结果文件
    with open(oldRetFile, 'r', encoding='utf-8') as f:
        oldRet = json.load(f, object_hook=custom_decoder)
    # 待合并文件
    with open(doRetFile, 'r', encoding='utf-8') as f:
        doRet = json.load(f)

    # 遍历待合并文件
    for doBook in doRet:

        # 从已有结果文件中查询是否包含item书籍
        flag = True
        for oldBook in oldRet:
            if isSameBook(book1=oldBook, book2=doBook):
                flag = False
                # 根据权重计算评分
                oldBook["appraise"] = calcAppraise(oldBook=oldBook, doBook=doBook, doWeight=doWeight)
                # 合并分类
                oldBook["classify"].extend(doBook["classify"])
                oldBook["classify"] = list(set(oldBook["classify"]))
                # 如果原有书籍简介、字数、文本大小、编辑状态、标签为空，则使用待合并书籍
                if oldBook["synopsis"] == "":
                    oldBook["synopsis"] = doBook["synopsis"]
                if oldBook["label"] == "":
                    oldBook["label"] = doBook["label"]
                if oldBook["wordCount"] == "":
                    oldBook["wordCount"] = doBook["wordCount"]
                if oldBook["txtSize"] == "":
                    oldBook["txtSize"] = doBook["txtSize"]
                if oldBook["editStatus"] == "":
                    oldBook["editStatus"] = doBook["editStatus"]
                
                # name, appraise, url, downloadUrl, collation, weight
                doWebSite = WebSite(doName, doBook["appraise"], doBook["url"], doBook["downloadUrl"], doBook["collation"], doWeight)
                oldBook["webSites"].append(doWebSite)
        if flag:
            doWebSite = WebSite(doName, doBook["appraise"], doBook["url"], doBook["downloadUrl"], doBook["collation"], doWeight)
            doBook["webSites"] = [doWebSite]
            oldRet.append(doBook)
    
    with open(retFile, 'w+', encoding='utf-8') as write_f:
	    write_f.write(json.dumps(oldRet, default=custom_encoder, ensure_ascii=False))

# 是否为同一本书籍
def isSameBook(book1, book2):
    if book1["title"] == book2["title"] and book1["author"] == book2["author"]:
        return True
    return False

# 根据权重计算评分
def calcAppraise(oldBook, doBook, doWeight):
    weightSum = doWeight
    for site in oldBook["webSites"]:
        weightSum += site.weight
    appraise = doBook["appraise"] * doWeight / weightSum
    for site in oldBook["webSites"]:
        appraise += site.appraise * site.weight / weightSum
    return appraise


if __name__ == '__main__':
    main()