import requests
import json


def main():
        
    with open("D:/workspace/loaf-on-job/02.code/lojob-spider/book/crawl-result-20240305100821.json", 'r', encoding='utf-8') as f:
        result = json.load(f)

    res = []
    for item in result:
        res.extend(list(set(item['classify'])))
    res = list(set(res))

    with open("D:/workspace/loaf-on-job/02.code/lojob-spider/book/crawl-qianyige-unique.json", 'w+', encoding='utf-8') as write_f:
	    json.dump(res, write_f, indent=4, ensure_ascii=False)
    
if __name__ == '__main__':
    main()
