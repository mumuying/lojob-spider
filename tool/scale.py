import json

# 初始小说评分
with open("D:/workspace/loaf-on-job/02.code/lojob-spider/book/zxcs-result.json", 'r', encoding='utf-8') as f:
    result = json.load(f)

max = 0
for item in result:
    if item['appraise'] > max:
        max = item['appraise']
print(max)

scale = 5 / max
print(scale)

for item in result:
    value = item['appraise'] * scale
    value = int(value * 1000) / 1000
    item['appraise'] = value
with open("D:/workspace/loaf-on-job/02.code/lojob-spider/book/zxcs.json", 'w+', encoding='utf-8') as write_f:
	json.dump(result, write_f, indent=4, ensure_ascii=False)
