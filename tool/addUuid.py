import uuid  

# 生成一个UUID  
unique_id = uuid.uuid4()  

# 如果需要字符串形式的UUID  
unique_id_str = str(uuid.uuid4()).replace('-', '')  