# 小机灵鬼阅读器-爬取小说

使用 scrapy 爬取小说数据，格式化数据之后，作为新小说网站的数据。可用于 scrapy 框架的学习。

## 网站演示

demo：[https://book.gremlins-book.com](https://book.gremlins-book.com)

本网站数据不再增加。

## 关联项目

爬取数据后，使用数据构建小说网站。

小说网站前端仓库：https://gitee.com/mumuying/lojob-nav.git

## 已支持网站

- 知轩藏书 https://zxcs.zip/
- 千夜阁 https://www.qianyege.com/
- 笔趣阁 https://www.biqugen.net/

在爬取上述网站过程中，已采用低速，尽量不影响网站正常使用。不再重新爬取以上网站。

**友情提示**，为了增加爬虫难度，不要使用有规律的 URL，上述网站的 URL 规律性强，缺少反扒措施。

## 环境

- python 版本：3.12.2
- 下载 git 上的代码，直接执行爬取操作即可

## 使用

- 安装 scrapy：pip install scrapy
- 创建项目[源代码中已有]：scrapy startproject book
- 进入项目[源代码中已有]：cd book
- 创建爬虫文件[源代码中已有]：scrapy genspider zxcs zxcs.zip
- 执行爬取操作：scrapy crawl zxcs

## 说明

执行爬取操作不报错，表明项目正常。可能出现的问题就是缺少库或者库版本不匹配。

### 通道

- 打开通道：在 settings.py 文件中解除 ITEM_PIPELINES 注释
- 每创建一个爬虫文件都需要添加一个通道
- 每创建一个爬虫文件都需要在 pippelines.py 文件中定义管道类

## 其他

小机灵鬼阅读器[上班摸鱼]：[https://www.gremlins-book.com](https://www.gremlins-book.com)<br>
小机灵鬼阅读博客：[https://blog.gremlins-book.com](https://blog.gremlins-book.com)
