def remove_start_end_quotation(s):  
    s = s.strip().strip('\n')
    # 如果字符串以分号开头，移除它  
    if s.startswith('"'):  
        s = s[1:]  
    # 如果字符串以分号结尾，移除它  
    if s.endswith('"'):  
        s = s[:-1]  
    return s