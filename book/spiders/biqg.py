import scrapy,json
import uuid  
from book.items import BookItem 


class BiqgSpider(scrapy.Spider):
    name = "biqg"
    allowed_domains = ["biqg.cc"]
    with open("D:/workspace/loaf-on-job/02.code/lojob-spider/book/biqg-xuanhuan-url.json", 'r', encoding='utf-8') as f:
        result = json.load(f)
    start_urls = result

    def parse(self, response):
        id = str(uuid.uuid4()).replace('-', '') 
        url = response.url
        title = response.xpath("//h1/text()").extract()
        author, editStatus, label, wordCount, collation, txtSize = '', '', '', '', '', ''
        author = response.xpath("/html/body/div[@class='book']/div[@class='info']/div[@class='small']/span[1]/text()").extract()
        synopsisF = response.xpath("/html/body/div[@class='book']/div[@class='info']/div[@class='intro']/dl/dd/text()").extract()
        synopsis = synopsisF[0].strip().replace('\xa0', '\n').replace('\u3000', '\n').strip('\n')
        classify = ['1']
        item = BookItem(id=id, url=url, title=title[0].strip(), author=author[0].strip(), synopsis=synopsis, classify=classify,
                        label=label, appraise=0.5, wordCount=wordCount, txtSize=txtSize, editStatus='连载', downloadUrl='', collation='')
        yield item

