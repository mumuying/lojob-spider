import scrapy
import json
import uuid 
from book.items import BookItem 
from book.tool import remove_start_end_quotation

# 知轩藏书爬虫
class ZxcsSpider(scrapy.Spider):
    # 爬虫的名字 一般运行爬虫的时候 使用的值
    name = "zxcs"
    # 允许访问的域名
    allowed_domains = ["zxcs.zip"]
    # 起始的url地址  指的是第一次要访问的域名
    # start_urls   是在allowed_domains的前面添加一个http：//
    #              是在allowed_domains的后面添加一个/
    # 如果以html结尾 就不用加/ 否则网站进不去  报错
    # start_urls = ["https://zxcs.zip/book/10461.html"]
    # 截至20240222，共10461本图书
    start_urls =  [ "https://zxcs.zip/book/" + str(i) + ".html" for i in range(1, 10462) ]

    # 是执行了start_urls之后  执行的方法
    # 方法中的response  就是返回的那个对象
    # 相当于 response = urllib.request.urlopen()
    #       response = requests.get()
    def parse(self, response):
        url = response.url
        titles = response.xpath('//a[@class="ng-star-inserted"]/text()').extract()
        title = titles[0] if titles else ''
        info = response.xpath('//p[@class="ng-star-inserted"]/text()').extract()
        author, synopsis, editStatus, classify, label, wordCount, collation, txtSize = '', '', '', '', '', '', '' ,''
        for it in info:
            it = it.strip()
            if it.startswith('【作者】：'):
                author = it[5:]
            elif it.startswith('【内容简介】：'):
                synopsis = it[7:]
            elif it.startswith('【状态】：'):
                editStatus = it[5:]
            elif it.startswith('【分类】：'):
                classify = it[5:]
            elif it.startswith('【标签】：'):
                label = it[5:]
            elif it.startswith('【字数】：'):
                wordCount = it[5:]
            elif it.startswith('【校对】：'):
                collation = it[5:]
            elif it.startswith('【TXT大小】：'):
                txtSize = it[7:]
            else:
                synopsis += it
        synopsis = remove_start_end_quotation(json.dumps(synopsis, ensure_ascii=False))
        downloadUrls = response.xpath('//a[@id="downloadtxt"]/@href').extract()
        downloadUrl = downloadUrls[0] if downloadUrls else ''
        appraiseAr = response.xpath('//div[@class="ng-star-inserted"]/text()').extract()
        appraise1, appraise2, appraise3, appraise4, appraise5 = appraiseAr[1], appraiseAr[3], appraiseAr[5], appraiseAr[7], appraiseAr[9]
        appraise = int(appraise1) * 5 + int(appraise2) * 4 + int(appraise3) * 3 + int(appraise4) * 2 + int(appraise5) * 1

        item = BookItem(url=url, title=title, author=author, synopsis=synopsis, editStatus=editStatus, classify=classify, 
                        label=label, wordCount=wordCount, collation=collation, txtSize=txtSize, 
                        downloadUrl=downloadUrl, appraise=appraise)
        yield item
