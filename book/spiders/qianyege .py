import scrapy
from book.items import BookItem 

# 抓取文本列表
class QianyegeSpiderBak(scrapy.Spider):
    name = "qianyege"
    allowed_domains = ["qianyege.com"]
    start_urls = ["https://qianyege.com"]

    # 起始的url地址  指的是第一次要访问的域名
    # start_urls   是在allowed_domains的前面添加一个http：//
    #              是在allowed_domains的后面添加一个/
    # 如果以html结尾 就不用加/ 否则网站进不去  报错
    # start_urls = ["https://zxcs.zip/book/10461.html"]
    # 截至20240222，共10461本图书
    
    # 1. 先抓取完本，一共162页 https://www.qianyege.com/fenlei-6-114.html
    start_urls =  [ "https://www.qianyege.com/fenlei-6-" + str(i) for i in range(0, 114) ]
    for index, element in enumerate(start_urls):
        start_urls[index] = element + ".html"

    def parse(self, response):
        url = response.url
        links = response.xpath('//span[@class="s2"]/a/@href').extract()

        classify = []
        for link in links:
            classify.append(link)
        
        item = BookItem(url=url, classify=classify)
        yield item
