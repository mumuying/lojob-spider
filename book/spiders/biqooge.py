import scrapy,json
from book.items import BookItem 


class BiqoogeSpider(scrapy.Spider):
    name = "biqooge"
    allowed_domains = ["biqooge.com"]
    start_urls = ["https://biqooge.com/"]

    with open("D:/workspace/loaf-on-job/02.code/lojob-spider/book/biqg-lishi-url.json", 'r', encoding='utf-8') as f:
        result = json.load(f)
    start_urls = result

    def parse(self, response):
        url = response.url
        links = response.xpath('//span[@class="s2"]/a/@href').extract()

        classify = []
        for link in links:
            classify.append(link)
        
        item = BookItem(url=url, classify=classify)
        yield item
