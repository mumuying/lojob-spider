import scrapy
from book.items import BookItem 


class BiqugenSpider(scrapy.Spider):
    name = "biqugen"
    allowed_domains = ["biqugen.net"]
    start_urls = ["https://biqugen.net"]
    start_urls =  [ "https://www.biqugen.net/quanben/" + str(i) for i in range(0, 726) ]

    def parse(self, response):
        url = response.url
        links = response.xpath('//a[@class="name"]/@href').extract()

        classify = []
        for link in links:
            classify.append(link)
        
        item = BookItem(url=url, classify=classify)
        yield item
