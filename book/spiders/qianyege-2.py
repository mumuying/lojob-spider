import scrapy
from book.items import BookItem 
import json
import uuid  
from book.tool import remove_start_end_quotation


class QianyegeSpider(scrapy.Spider):
    name = "qianyege"
    allowed_domains = ["qianyege.com"]
    start_urls = ["https://qianyege.com"]

    # 起始的url地址  指的是第一次要访问的域名
    # start_urls   是在allowed_domains的前面添加一个http：//
    #              是在allowed_domains的后面添加一个/
    # 如果以html结尾 就不用加/ 否则网站进不去  报错
    # start_urls = ["https://zxcs.zip/book/10461.html"]
    # 截至20240222，共10461本图书
    
    with open("D:/workspace/loaf-on-job/02.code/lojob-spider/book/crawl-qianyege-unique.json", 'r', encoding='utf-8') as f:
        result = json.load(f)
    start_urls = result

    def parse(self, response):
        id = str(uuid.uuid4()).replace('-', '') 
        url = response.url
        title = response.xpath('//h1/text()').extract()
        info = response.xpath('//div[@id="info"]/p/text()').extract()
        author, editStatus, label, wordCount, collation, txtSize = '', '', '', '', '', ''
        author = info[0][5:]
        synopsisF = response.xpath('//div[@id="intro"]/p/text()').extract()
        synopsis = synopsisF[0].strip().replace('\xa0', '\n').replace('\u3000', '\n').strip('\n')
        synopsis = remove_start_end_quotation(json.dumps(synopsis, ensure_ascii=False))
        footCont = response.xpath('//div[@class="footer_cont"]/p/text()').extract()[0]
        classify = ['5']
        # if '玄幻' in footCont or '奇幻' in footCont: 
        #     classify.append('1')
        # if "武侠" in footCont or '仙侠' in footCont:
        #     classify.append('2')
        # if "都市" in footCont or '言情' in footCont:
        #     classify.append('3')
        # if "历史" in footCont or '军事' in footCont:
        #     classify.append('4')
        # if "游戏" in footCont or '体育' in footCont:
        #     classify.append('5')
        # if "科幻" in footCont or '悬疑' in footCont:
        #     classify.append('6')
        # if "穿越" in footCont or '架空' in footCont:
        #     classify.append('7')
        # if "轻小说" in footCont:
        #     classify.append('8')
        # if len(classify) == 0:
        #     classify.append('9')

        item = BookItem(id=id, url=url, title=title[0].strip(), author=author.strip(), synopsis=synopsis, classify=classify,
                        label=label, appraise=0.5, wordCount=wordCount, txtSize=txtSize, editStatus='完本', downloadUrl='', collation='')
        yield item
