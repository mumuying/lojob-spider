# Define here the models for your scraped items 定义需要爬取的内容
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class BookItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    # ID
    id = scrapy.Field()
    # 网址
    url = scrapy.Field()
    # 书籍标题
    title = scrapy.Field()
    # 作者
    author = scrapy.Field()
    # 简介、提要
    synopsis = scrapy.Field()
    # 编辑状态
    editStatus = scrapy.Field()
    # 分类
    classify = scrapy.Field()
    # 标签
    label = scrapy.Field()
    # 字数
    wordCount = scrapy.Field()
    # 校对
    collation = scrapy.Field()
    # TXT大小
    txtSize = scrapy.Field()
    # 下载地址
    downloadUrl = scrapy.Field()
    # 评分
    appraise = scrapy.Field()
