# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
from itemadapter import ItemAdapter
import re
import datetime
import os;


# 管道存储爬取内容
class BookPipeline:
    def open_spider(self, spider):
        self.fp = open('book.json', 'w+', encoding='utf-8')
        self.fp.write("[")

    def process_item(self, item, spider):
        self.fp.write(str(item) + "\n,")
        return item
    
    def close_spider(self, spider):
        self.fp.write("]")
        self.fp.flush()
        self.fp.seek(os.SEEK_SET)# 设置指针回到文件最初

        time1 = datetime.datetime.now().strftime('%Y%m%d%H%M%S')# 只取年月日时分秒
        f2 = open('crawl-result-' + time1 + '.json', 'w+', encoding='utf-8')
        str1 = r'\''
        str2 = r'"'
        for ss in self.fp.readlines():
            tt = re.sub(str1, str2, ss)
            f2.write(tt)

        f2.close()
        self.fp.close()
