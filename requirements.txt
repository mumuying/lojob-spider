absl-py==2.1.0
astunparse==1.6.3
attrs==23.2.0
Automat==22.10.0
certifi==2024.2.2
cffi==1.16.0
charset-normalizer==3.3.2
constantly==23.10.4
cryptography==42.0.4
cssselect==1.2.0
filelock==3.13.1
flatbuffers==24.3.25
gast==0.5.4
google-pasta==0.2.0
grpcio==1.62.1
h5py==3.10.0
hyperlink==21.0.0
idna==3.6
incremental==22.10.0
itemadapter==0.8.0
itemloaders==1.1.0
jmespath==1.0.1
keras==3.1.1
libclang==18.1.1
lxml==5.1.0
Markdown==3.6
markdown-it-py==3.0.0
MarkupSafe==2.1.5
mdurl==0.1.2
ml-dtypes==0.3.2
namex==0.0.7
numpy==1.26.4
opt-einsum==3.3.0
optree==0.11.0
packaging==23.2
parsel==1.8.1
Protego==0.3.0
protobuf==4.25.3
pyasn1==0.5.1
pyasn1-modules==0.3.0
pycparser==2.21
PyDispatcher==2.0.7
Pygments==2.17.2
pyOpenSSL==24.0.0
queuelib==1.6.2
requests==2.31.0
requests-file==2.0.0
rich==13.7.1
Scrapy==2.11.1
service-identity==24.1.0
setuptools==69.1.0
six==1.16.0
tensorboard==2.16.2
tensorboard-data-server==0.7.2
tensorflow==2.16.1
tensorflow-intel==2.16.1
termcolor==2.4.0
tldextract==5.1.1
Twisted==23.10.0
twisted-iocpsupport==1.0.4
typing_extensions==4.9.0
urllib3==2.2.1
w3lib==2.1.2
Werkzeug==3.0.1
wheel==0.43.0
wrapt==1.16.0
zope.interface==6.2
